#include <stdio.h>

void firstRun(int *actionCounter, char check, char (*actions)[100])
{
    while (check != 'S' && check != 's')
    {
        printf("Am I in front of a wall?\n");
        printf("What should I do?\n");
        do {
            scanf(" %c", &check);
        } while (getchar() != '\n');  // Löscht das newline-Zeichen aus dem Eingabepuffer

        switch (check)
        {
        case 'W':
        case 'w':
            printf("I made a step forward.\n");
            (*actions)[(*actionCounter)++] = 'W';
            break;
        case 'A':
        case 'a':
            printf("I made a turn left.\n");
            (*actions)[(*actionCounter)++] = 'A';
            break;
        case 'D':
        case 'd':
            printf("I made a turn right.\n");
            (*actions)[(*actionCounter)++] = 'D';
            break;
        default:
            if (check != 'S' && check != 's')
            {
                printf("This was not a valid input!\n");
            } else {
                (*actions)[(*actionCounter)++] = 'S';
            }
            break;
        }
    }
}
