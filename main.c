#include <stdio.h>

void intro();
void firstRun(int *, char, char (*)[100]);
char loopRun(char, int, int, char (*)[100]);

int selfRuns = 0;

int main()
{
    char check;
    char actions[100];
    int actionCounter = 0;
    char cancalation;

    intro();
    firstRun(&actionCounter, check, &actions);
    while (cancalation != 'E')
    {
        cancalation = loopRun(check, selfRuns, actionCounter, &actions);
    }

    return 0;
}
