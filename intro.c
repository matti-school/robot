#include <stdio.h>

void intro()
{
    printf("Welcome\n");
	printf("This Robot will take the following inputs from you: \n");
	printf("\"W\" or \"w\" for forward\n");
	printf("\"A\" or \"a\" for left turn\n");
	printf("\"D\" or \"d\" for right turn\n");
	printf("\"S\" or \"s\" for stop\n");
	printf("Please select stop if I am at the starting point or if something goes wrong.\n");
}