# Schulprojektprojekt Robot

Diese Anwendung wird im Rahmen der Ausbildung zum Fachinformatiker für Anwendungsentwicklung verfasst.
Sie dient als Grundprojekt zum Lernen, Üben und ausprobieren, im Rahmen der Ausbildung.

## Voraussetzungen

Im Rahmen des Berufsschulunterrichts soll ein Programm erstellt werden, welches einen »Roboter« durch den Raum laufen lässt.

Als zusätzliche Funktion habe ich genommen, dass der »Roboter« den Kurs selbst absolvieren kann, indem er die Eingaben des Nutzers einfach wiederholt.

Möchte man den Code Lokal ausführen so sind folgende Dinge von nöten:

    - GIT zum pullen der Anwendung
    - entsprechende Berechtigungen zum pullen
    - gcc compiler

## Lokal einrichten und Starten

Folgende befehle, sind in der Bash console auszuführen:

```bash
git pull git@gitlab.com:matti-school/robot.git

cd robot

make -f robot_src.m
```

Um das Programm zu Starten gibt man folgendes ein:

```bash
./robot
```

## Zum Projekt als Tester beitragen

Ich heiße Feedback und konstruktive Kritik jederzeit willkommen.

Feedback bzw. Kritik kann sowohl als DM als auch ober das Issue Feature von Gitlab eingereicht werden.