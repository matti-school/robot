robot: main.o intro.o firstRun.o loopRun.o
	cc -o robot main.o intro.o firstRun.o loopRun.o

main.o: main.c
	cc -c main.c

intro.o: intro.c
	cc -c intro.c

firstRun.o: firstRun.c
	cc -c firstRun.c

loopRun.o: loopRun.c
	cc -c loopRun.c