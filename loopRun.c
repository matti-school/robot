#include <stdio.h>

char loopRun(char check, int selfRuns, int actionCounter, char (*actions)[100])
{
    char boolQuestion;

    switch (selfRuns)
    {
    default:
        printf("I stopped.\n");
        break;
    case 0:
        printf("I stopped.\n");
        printf("Thank you for guiding me!\n");
        printf("You made %d valid inputs!\n", actionCounter);
        printf("Do you want a list of all your inputs? (\"Y\" = Yes | \"N\" = No | \"E\" = Exit)\n");
        do {
            scanf(" %c", &boolQuestion);
        } while (getchar() != '\n');

        if (boolQuestion == 'Y' || boolQuestion == 'y')
        {
            int listCounter = 0;
            printf("Your inputs:\n");
            while (listCounter < actionCounter)
            {
                printf("%c ", (*actions)[listCounter]);
                listCounter++;
            }
            printf("\nThat's all.\n");
        }
        if (boolQuestion == 'E' || boolQuestion == 'e') {
            return 'E';
        }
        break;
    }

    printf("Should I do this round again on my own? (\"Y\" = Yes | \"N\" = No | \"E\" = Exit)\n");
    do {
        scanf(" %c", &boolQuestion);
        if (boolQuestion == 'Y' || boolQuestion == 'y')
        {
            selfRuns++;
            int listCounter = 0;
            while (listCounter < actionCounter)
            {
                switch ((*actions)[listCounter])
                {
                case 'W':
                case 'w':
                    printf("I made a step forward.\n");
                    break;
                case 'A':
                case 'a':
                    printf("I made a turn left.\n");
                    break;
                case 'D':
                case 'd':
                    printf("I made a turn right.\n");
                    break;
                case 'S':
                case 's':
                    printf("I stopped.\n");
                    break;
                }
                listCounter++;
            }
        }
        if (boolQuestion == 'E' || boolQuestion == 'e') {
            return 'E';
        } else {
            return 'N';
        }
    } while (check != 'E' && check != 'e');
}